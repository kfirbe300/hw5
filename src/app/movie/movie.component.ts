import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
@Input() data:any;
@Output() deleteMe = new EventEmitter<any>();

id;
title;
studio;
income;


deleteRow() {
  console.log("deleted "+ this.title)
  this.deleteMe.emit(this);
}

  constructor() { }

  ngOnInit() {
    this.id = this.data.id;
    this.title = this.data.title;
    this.studio = this.data.studio;
    this.income = this.data.income;
  }

}